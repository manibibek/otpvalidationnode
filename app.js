var express = require('express')
var app = express();

app.use(express.json());
app.post('/submitOtp', (req, res) => {
    let otp = req.body.otp;
    otp = String(otp).split("");
    if (otp.length !== 6 || otp.slice(-1)[0] == 7) {
        res.send({"status": "error"})
    }
    res.send({ "status": "success" });
});

const port = process.env.PORT || 3000;
app.listen(port, () => {
  
});